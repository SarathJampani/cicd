
package com.java;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Alert;

import com.java.importnexport.ImportConfigDetails;
import com.java.objects.*;
import java.util.concurrent.TimeUnit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.SecureRandom;
import java.sql.Timestamp;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.ArrayList;
import com.java.objects.TestDataDetails;




public class AppTestType {
	;
	static WebDriverWait wait = null;

	public enum AppKeyWords {
		SELECTDEFAULTFRAME, DRDWDEFAULTVALUE, STORECONSULTATIONDETAILS, CLIENTSELECTIONINVOICEPAGE, SCROLLDOWN, JCLICK, VERIFYALERTTEXT, ENTERUNIQUETEXT, ENTERSTOREDVALUE, SWITCHWINDOW, CLOSESCREEN, ENTEREMAILID, ENTERSTOREDEMAIL, ISDISPLAYED, ISSELECTED, INPUTSUGGESTIONSELECTION, VERIFYTEXT, DRAGANDDROPFUNC, SWITCHTOFRAME, ALERTACCEPT, ALERTDISMISS, ALERTGETTEXT, HOVERANDCLICK, HOVER, COMPARETEXT, CURRENTDATECHECK, PAGEREFRESH, SPECIFICWAIT, IMPWAIT, IMPWAIT2, IMPWAIT3, IMPWAIT4, IMPWAIT5, ISNOTDISPLAYED, COPYANDPASTE, STORENUMBER, STORETEXT, CLOSEBROWSER, UNCHECKCHECK, SPECIFICCLICK, UNIQUEALPHATEXT, ENTERALPHAVALUE, IDCREATOR
	,SCHEDULE
	};

	TestType tt;

	public AppTestType(TestType tt) {
		this.tt = tt;

	}

	public static ResultDetails resultDetails = new ResultDetails();

	public ResultDetails performAction(WebDriver webdriver, String fieldText, String value, String action,
			String fieldName) {

		try {

			AppKeyWords keys = AppKeyWords.valueOf(action.toUpperCase());

			WebDriverWait wait = new WebDriverWait(webdriver, 60);

			switch (keys) {
			case CLIENTSELECTIONINVOICEPAGE:
				try {
					String[] xpath = fieldText.split(";", 2);
					WebElement actuaClientName = webdriver.findElement(By.xpath(xpath[0]));
					WebElement actualOrgName = webdriver.findElement(By.xpath(xpath[1]));

					String clientName = actuaClientName.getText();
					String orgName = actualOrgName.getText();

					String clientDropdown = clientName + " (" + orgName + ")";

					System.out.println(clientDropdown + " Test");

					tt.driver.hMap.put("client", clientDropdown);
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;
			case STORECONSULTATIONDETAILS:
				try {
					String[] xpath = fieldText.split(";", 4);
					WebElement actualrequestNumber = webdriver.findElement(By.xpath(xpath[0]));
					WebElement actualrequestName = webdriver.findElement(By.xpath(xpath[1]));
					WebElement actualConsultationDate = webdriver.findElement(By.xpath(xpath[2]));
					WebElement actualconsultationName = webdriver.findElement(By.xpath(xpath[3]));

					String requestNumber = actualrequestNumber.getText().split("\\s")[1].trim();
					String requestName = actualrequestName.getText();
					String consultationDate = actualConsultationDate.getText();
					String consultationTime = actualconsultationName.getText();

					String consultationName = requestNumber + " - " + requestName + " (" + consultationDate + " @" + " "
							+ consultationTime + " EST)";

					System.out.println(consultationName + " Test");

					tt.driver.hMap.put("request", consultationName);
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;
			case DRDWDEFAULTVALUE:
				try {
					WebElement elementDrdp = webdriver.findElement(By.xpath(fieldText));
					Select select = new Select(elementDrdp);
					String elementDrdpValue = select.getFirstSelectedOption().getText();
					String expText = tt.driver.utils.getValue(value);
					if (elementDrdpValue.equals(expText)) {
						resultDetails.setFlag(true);
					}
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;
			case SELECTDEFAULTFRAME:
				tt.driver.utils.getValue(value);
				webdriver.switchTo().defaultContent();
				resultDetails.setFlag(true);
				break;

			case JCLICK:
				try {
					WebElement elescr = webdriver.findElement(By.xpath(fieldText));

					((JavascriptExecutor) webdriver).executeScript("arguments[0].click()", elescr);

					resultDetails.setFlag(true);
				} catch (Exception ex) {

					resultDetails.setFlag(false);

					ex.printStackTrace();

				}

				break;
			case ENTERSTOREDVALUE:
				try {
					value = tt.driver.utils.getValue(value);
					String uniqueText = tt.driver.hMap1.get(value);
					webdriver.findElement(By.xpath(fieldText)).clear();
					webdriver.findElement(By.xpath(fieldText)).sendKeys(uniqueText);

					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;
			case ENTERUNIQUETEXT:
				try {
					String expText = tt.driver.utils.getValue(value);
					String uniqueText = expText + (new Timestamp(System.currentTimeMillis()).getTime());
					JavascriptExecutor js = (JavascriptExecutor) webdriver;

					js.executeScript("arguments[0].setAttribute('value','" + uniqueText + "');",
							webdriver.findElement(By.xpath(fieldText)));
					String a = Integer.toString(tt.driver.l);
					tt.driver.hMap1.put("textKey" + a, uniqueText);
					tt.driver.l++;
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case VERIFYALERTTEXT:
				try {

					Alert alert = webdriver.switchTo().alert();
					String alert_Text = alert.getText();
					System.out.println(alert_Text);
					if (value.equalsIgnoreCase(alert_Text)) {
						System.out.println("Texts are matched");
						resultDetails.setFlag(true);
					} else {
						resultDetails.setFlag(false);
					}
				} catch (Exception ex) {
					resultDetails.setFlag(false);
					ex.printStackTrace();

				}

				break;
			case UNIQUEALPHATEXT:
				try {
					String ALPHABET = "ABCDEFGHIKKJKL";
					SecureRandom RANDOM = new SecureRandom();
					StringBuilder sb = new StringBuilder();
					for (int i = 0; i < 10; ++i) {
						sb.append(ALPHABET.charAt(RANDOM.nextInt(ALPHABET.length())));
					}
					sb.toString();
					String expText = tt.driver.utils.getValue(value);
					String uniqueAlpha = expText + sb;
					JavascriptExecutor js = (JavascriptExecutor) webdriver;

					js.executeScript("arguments[0].setAttribute('value','" + uniqueAlpha + "');",
							webdriver.findElement(By.xpath(fieldText)));
					String a = Integer.toString(tt.driver.j);
					tt.driver.hMap1.put("alpha" + a, uniqueAlpha);
					tt.driver.j++;
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;
			case ENTERALPHAVALUE:
				try {
					value = tt.driver.utils.getValue(value);
					String uniqueAlpha = tt.driver.hMap1.get(value);
					webdriver.findElement(By.xpath(fieldText)).clear();
					webdriver.findElement(By.xpath(fieldText)).sendKeys(uniqueAlpha);

					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case SCROLLDOWN:

				try {

					// webdriver.findElement(By.id(fieldText)).sendKeys(Keys.PAGE_DOWN);

					WebElement elescr = webdriver.findElement(By.xpath(fieldText));

					((JavascriptExecutor) webdriver).executeScript("arguments[0].scrollIntoView(true);", elescr);

					resultDetails.setFlag(true);

				} catch (Exception ex) {
					resultDetails.setFlag(false);
					ex.printStackTrace();

				}

				break;

			case SWITCHWINDOW:
				try {
					List<String> handles = new ArrayList<String>(webdriver.getWindowHandles());

					for (String handle : handles) {
						webdriver.switchTo().window(handle);
					}
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case CLOSESCREEN:
				try {

					webdriver.close();
					resultDetails.setFlag(true);
				}

				catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case ENTEREMAILID:
				try {
					String expText = tt.driver.utils.getValue(value);
					String uniqueText = expText + (new Timestamp(System.currentTimeMillis()).getTime());
					String UniqueEmail = uniqueText + "@automation.in";
					JavascriptExecutor js = (JavascriptExecutor) webdriver;
					js.executeScript("arguments[0].setAttribute('value','" + UniqueEmail + "');",
							webdriver.findElement(By.xpath(fieldText)));
					String a = Integer.toString(tt.driver.m);
					tt.driver.hMap1.put("textkeyEmail" + a, UniqueEmail);
					tt.driver.m++;
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case ENTERSTOREDEMAIL:
				try {
					value = tt.driver.utils.getValue(value);
					String UniqueEmail = tt.driver.hMap1.get(value);
					webdriver.findElement(By.xpath(fieldText)).clear();
					webdriver.findElement(By.xpath(fieldText)).sendKeys(UniqueEmail);
					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case ISDISPLAYED:
				try {
					WebElement isdis = webdriver.findElement(By.xpath(fieldText));
					isdis.isDisplayed();
					resultDetails.setFlag(true);
				} catch (Exception ex) {
					resultDetails.setFlag(false);
					ex.printStackTrace();
				}
				break;

			case ISSELECTED:
				try {
					WebElement issel = webdriver.findElement(By.xpath(fieldText));
					issel.isSelected();
					resultDetails.setFlag(true);
				} catch (Exception ex) {
					resultDetails.setFlag(false);
					ex.printStackTrace();
				}
				break;

			case INPUTSUGGESTIONSELECTION:
				try {
					Actions actions = new Actions(webdriver);
					int convertValue = Integer.parseInt(tt.driver.utils.getValue(value));
					for (int i = 0; i <= convertValue; i++) {
						actions.sendKeys(Keys.DOWN).build().perform();
					}
					actions.sendKeys(Keys.ENTER).build().perform();
					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case VERIFYTEXT:
				try {
					WebElement element = webdriver.findElement(By.xpath(fieldText));
					String elementValue = element.getText();
					if (!elementValue.isEmpty()) {
						resultDetails.setFlag(true);
					}
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case DRAGANDDROPFUNC:
				try {
					String[] xpath = fieldText.split(";", 2);
					WebElement From = webdriver.findElement(By.xpath(xpath[0]));
					WebElement To = webdriver.findElement(By.xpath(xpath[1]));
					Actions actions = new Actions(webdriver);
					actions.dragAndDrop(From, To).build().perform();
					resultDetails.setFlag(true);
				}

				catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case SWITCHTOFRAME:
				try {
					WebElement frameId = webdriver.findElement(By.xpath(fieldText));
					webdriver.switchTo().frame(frameId);
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case ALERTACCEPT:
				try {
					Alert alert = webdriver.switchTo().alert();
					Thread.sleep(1000);
					alert.accept();
					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case ALERTDISMISS:
				try {
					Alert alert = webdriver.switchTo().alert();
					Thread.sleep(1000);
					alert.dismiss();
					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case ALERTGETTEXT:
				try {
					Alert alert = webdriver.switchTo().alert();
					Thread.sleep(1000);
					String alertValue = alert.getText();
					if (!alertValue.isEmpty()) {
						resultDetails.setFlag(true);
					}
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case HOVERANDCLICK:

				try {

					WebElement element = webdriver.findElement(By.xpath(fieldText));
					Actions actions = new Actions(webdriver);
					actions.moveToElement(element);
					actions.click().build().perform();
					resultDetails.setFlag(true);
				}

				catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case HOVER:
				try {

					WebElement element = webdriver.findElement(By.xpath(fieldText));
					Actions actions = new Actions(webdriver);
					actions.moveToElement(element).build().perform();
					resultDetails.setFlag(true);
				}

				catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case COMPARETEXT:
				try {
					value = tt.driver.utils.getValue(value);
					String actualValue = webdriver.findElement(By.xpath(fieldText)).getText();
					if (value.equalsIgnoreCase(actualValue) || value.contains(actualValue)) {
						resultDetails.setFlag(true);
					} else if (value.isEmpty()) {
						resultDetails.setFlag(false);
					}
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case CURRENTDATECHECK:
				try {
					String givedate = webdriver.findElement(By.xpath(fieldText)).getAttribute("value");
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
					Date date = new Date();

					if (givedate.equalsIgnoreCase(formatter.format(date))) {
						resultDetails.setFlag(true);
					}

					else {
						resultDetails.setFlag(false);
					}
					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case PAGEREFRESH:
				try {
					webdriver.navigate().to(webdriver.getCurrentUrl());
					resultDetails.setFlag(true);
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case SPECIFICWAIT:
				try {
					WebDriverWait wait1 = new WebDriverWait(webdriver, 120);
					WebElement element = wait1
							.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(fieldText)));
					resultDetails.setFlag(true);
				} catch (Exception ex) {
					resultDetails.setFlag(false);
					ex.printStackTrace();
				}
				break;

			case SPECIFICCLICK:
				try {
					WebDriverWait wait1 = new WebDriverWait(webdriver, 120);
					WebElement element = wait1.until(ExpectedConditions.elementToBeClickable(By.xpath(fieldText)));
					resultDetails.setFlag(true);
				} catch (Exception ex) {
					resultDetails.setFlag(false);
					ex.printStackTrace();
				}
				break;

			case IMPWAIT:
				try {
					webdriver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
					webdriver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
					resultDetails.setFlag(true);
				} catch (Exception ex) {

					resultDetails.setFlag(false);

					ex.printStackTrace();

				}

				break;

			case IMPWAIT2:
				try {
					webdriver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					// webdriver.manage().timeouts().pageLoadTimeout(100,TimeUnit.SECONDS);
					resultDetails.setFlag(true);
				} catch (Exception ex) {

					resultDetails.setFlag(false);

					ex.printStackTrace();

				}

				break;

			case IMPWAIT3:
				try {
					webdriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
					// webdriver.manage().timeouts().pageLoadTimeout(100,TimeUnit.SECONDS);
					resultDetails.setFlag(true);
				} catch (Exception ex) {

					resultDetails.setFlag(false);

					ex.printStackTrace();

				}

				break;

			case IMPWAIT4:
				try {
					webdriver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
					// webdriver.manage().timeouts().pageLoadTimeout(100,TimeUnit.SECONDS);
					resultDetails.setFlag(true);
				} catch (Exception ex) {

					resultDetails.setFlag(false);

					ex.printStackTrace();

				}

				break;

			case IMPWAIT5:
				try {
					webdriver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
					// webdriver.manage().timeouts().pageLoadTimeout(100,TimeUnit.SECONDS);
					resultDetails.setFlag(true);
				} catch (Exception ex) {

					resultDetails.setFlag(false);

					ex.printStackTrace();

				}

				break;

			case ISNOTDISPLAYED:
				try {
					WebElement isdis = webdriver.findElement(By.xpath(fieldText));
					isdis.isDisplayed();
					resultDetails.setFlag(false);
				} catch (Exception ex) {
					resultDetails.setFlag(true);
					ex.printStackTrace();
				}
				break;

			case COPYANDPASTE:
				try {
					String[] xpath = fieldText.split(";", 2);
					String copyValue = webdriver.findElement(By.xpath(xpath[0])).getText();
					if (!copyValue.isEmpty()) {
						webdriver.findElement(By.xpath(xpath[1])).sendKeys(copyValue);
						resultDetails.setFlag(true);
					} else {
						resultDetails.setFlag(false);
					}
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case STORENUMBER:
				try {

					String textValue = webdriver.findElement(By.xpath(fieldText)).getText();
					String requestID = textValue.split("\\#")[1].trim();

					tt.driver.hMap.put("requestID", requestID);
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case STORETEXT:
				try {

					String storeValue = webdriver.findElement(By.xpath(fieldText)).getText();
					tt.driver.hMap.put("storeValue", storeValue);
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case IDCREATOR:
				try {
					String[] xpath = fieldText.split(";", 3);
					WebElement requestID = webdriver.findElement(By.xpath(xpath[0]));
					String rowNO = xpath[1];
					int rowNumber = Integer.parseInt(rowNO);
					String columnNO = xpath[2];
					int columnNumber = Integer.parseInt(columnNO);

					String requestIDValue = requestID.getText();

					String configFile = "Config.xml";

					ArrayList<ConfigDetails> confgDtls = new ArrayList<ConfigDetails>();
					ImportConfigDetails impCnf = new ImportConfigDetails(configFile);

					confgDtls = impCnf.readConfigData();
					ConfigDetails configDetails = confgDtls.get(0);

					String filepath = System.getProperty("user.dir") + "/../UtilityFile" + "/"
							+ configDetails.getTestDataSource();
					//System.out.println("Data file path" + filepath);
					
					try {
						FileInputStream inputStream = new FileInputStream(new File(filepath));
						Workbook workbook = WorkbookFactory.create(inputStream);
						Sheet sheet = workbook.getSheet("DataSheet");
						Row row = sheet.getRow(rowNumber);

						row.createCell(columnNumber).setCellValue(requestIDValue);
						
						inputStream.close();

						FileOutputStream fileOut = new FileOutputStream(filepath);
						workbook.write(fileOut);
						fileOut.close();
						workbook.close();
					} catch (Exception e) {
						System.out.println("!!!!!!!! TestData file is not found !!!!!!!!");
						resultDetails.setFlag(false);
					}
				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;
				
			case SCHEDULE:
				try {
					String slt = fieldText;
					if(slt.equals("SLT") || slt.equals("slt"))
					{
						Random rand = new Random();
				        int val = rand.nextInt(300);  //1 to 299
				        String xpath= "(//*[starts-with(@id,'slot')])[" + val + "]";
				        webdriver.findElement(By.xpath(xpath.trim())).click();
						resultDetails.setFlag(true);	
					}
					else
					{
					resultDetails.setFlag(false);
					}

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			case CLOSEBROWSER:
				try {

					tt.driver.utils.getValue(value);
					webdriver.quit();
					resultDetails.setFlag(true);

				} catch (Exception e) {
					resultDetails.setFlag(false);
				}
				break;

			}
		} catch (IllegalArgumentException e) {
			resultDetails.setErrorMessage(" Unable to find Element");
			System.out.println(e.getMessage());	
		} catch (Exception e) {
			resultDetails.setErrorMessage(" Exception in executing the action " + action);
			System.out.println(e.getMessage());	
		}
		return resultDetails;
	}

}
