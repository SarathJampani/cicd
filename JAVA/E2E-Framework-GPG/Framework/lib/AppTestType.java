
package com.java;



import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.Alert;
import com.java.objects.*;
//import org.junit.Assert.*;
import java.util.concurrent.TimeUnit;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.ArrayList;



public class AppTestType   {
	
	static WebDriverWait wait=null;
	
	public enum AppKeyWords {
		 SELECTDEFAULTFRAME,FLIGHTSELECTION,VERIFYPOPUP, SCROLLDOWN,JCLICK, VERIFYALERTTEXT, ENTERUNIQUETEXT,ENTERSTOREDVALUE, SWITCHWINDOW, CLOSESCREEN, ENTEREMAILID,ENTERSTOREDEMAIL, ISDISPLAYED, ISSELECTED, INPUTSUGGESTIONSELECTION, VERIFYTEXT,
		 DRAGANDDROPFUNC,SWITCHTOFRAME, ALERTACCEPT, ALERTDISMISS, ALERTGETTEXT,HOVERANDCLICK,HOVER,COMPARETEXT,CURRENTDATECHECK, PAGEREFERSH, SPECIFICWAIT, IMPWAIT, ISNOTDISPLAYED


	};
	

	TestType tt;

	public AppTestType(TestType tt) {
		this.tt = tt;
	
	}

	public static ResultDetails resultDetails = new ResultDetails();

	public ResultDetails performAction(WebDriver webdriver, String fieldText,String value, String action, String fieldName) {

		try {

			AppKeyWords keys = AppKeyWords.valueOf(action.toUpperCase());
			
			WebDriverWait wait=new WebDriverWait(webdriver,60);
		
			switch (keys) {
			case SELECTDEFAULTFRAME:
				tt.driver.utils.getValue(value);
				webdriver.switchTo().defaultContent();
				resultDetails.setFlag(true);
				break;
				
				case FLIGHTSELECTION:
				try {
					FlightSelection(webdriver,fieldText, value);
				} catch (Throwable e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
				case JCLICK:
				try{
					 WebElement elescr = webdriver.findElement(By.xpath(fieldText));

                    ((JavascriptExecutor) webdriver).executeScript("arguments[0].click()", elescr);

                     resultDetails.setFlag(true);
				  } catch (Exception ex) {

                                  resultDetails.setFlag(false);

                                  ex.printStackTrace();

                           }

                           break;
				case ENTERSTOREDVALUE:
				try{
					value = tt.driver.utils.getValue(value);
					String uniqueText = tt.driver.hMap.get(value);
					webdriver.findElement(By.xpath(fieldText)).clear();
					webdriver.findElement(By.xpath(fieldText)).sendKeys(uniqueText);
					
					 resultDetails.setFlag(true);
				}catch(Exception e){
					
				}
				break;
				case ENTERUNIQUETEXT:
				try{
				String expText = tt.driver.utils.getValue(value);
				String uniqueText = expText+(new Timestamp(System.currentTimeMillis()).getTime());
				JavascriptExecutor js = (JavascriptExecutor)webdriver;
     

				js.executeScript("arguments[0].setAttribute('value','" + uniqueText + "');", webdriver.findElement(By.xpath(fieldText)));
     
   
					tt.driver.hMap.put("textKey",uniqueText);
					resultDetails.setFlag(true);
     
    }catch(Exception e){
      resultDetails.setFlag(false);
    }
				break;
				case VERIFYALERTTEXT:
				try{
					
					
					Alert alert = webdriver.switchTo().alert();
					String alert_Text = alert.getText();
					System.out.println(alert_Text);
					if(value.equalsIgnoreCase(alert_Text)){
						System.out.println("Texts are matched");
						 resultDetails.setFlag(true);
					}
					
				}catch (Exception ex) {

                                  resultDetails.setFlag(false);

                                  ex.printStackTrace();

                           }
				
				break;
				
				case SCROLLDOWN:

                           try {

                                  // webdriver.findElement(By.id(fieldText)).sendKeys(Keys.PAGE_DOWN);

                                  WebElement elescr = webdriver.findElement(By.xpath(fieldText));

                                  ((JavascriptExecutor) webdriver).executeScript("arguments[0].scrollIntoView(true);", elescr);

                                  resultDetails.setFlag(true);

 

                           } catch (Exception ex) {

                                  resultDetails.setFlag(false);

                                  ex.printStackTrace();

                           }

                           break;
				
				
				case VERIFYPOPUP:
                                  try
                                  {
                                  Thread.sleep(1000);
								  for(int i=0;i<=25;i++)
								  {
									  List<WebElement> selectregion = webdriver.findElements(By
													.xpath("//button[@class='modalCloseImg simplemodal-close']"));
									  int region = selectregion.size();
									  //System.out.println(region);
									  if (region >= 1) {
											 webdriver
											 .findElement(
														   By.xpath("//button[@class='modalCloseImg simplemodal-close']"))
														   .click();
										break;
									  }
									  //System.out.println("looping next");
									  Thread.sleep(1000);
								  }
                                  resultDetails.setFlag(true);
                                  }catch(Exception e)
                                  {
                                         resultDetails.setFlag(false);
                                  }
                                  break;
						case SWITCHWINDOW:
						try{
							List<String> handles = new ArrayList<String>(webdriver.getWindowHandles());
							
							for(String handle: handles){
								webdriver.switchTo().window(handle);
							}
							resultDetails.setFlag(true);
							
						}catch(Exception e){
							resultDetails.setFlag(false);
						}
		           break;
				   
				   case CLOSESCREEN:
				   try{
					   
					  webdriver.close();
					  resultDetails.setFlag(true);
				   }
				   
				   catch(Exception e){
							resultDetails.setFlag(false);
						}
					break;	
					
			case ENTEREMAILID:
			try{
				String expText = tt.driver.utils.getValue(value);
				String uniqueText = expText+(new Timestamp(System.currentTimeMillis()).getTime());
				String UniqueEmail = uniqueText +"@automation.in";
				JavascriptExecutor js = (JavascriptExecutor)webdriver;
				js.executeScript("arguments[0].setAttribute('value','" + UniqueEmail + "');", webdriver.findElement(By.xpath(fieldText)));
				tt.driver.hMap.put("textKey",UniqueEmail);
				resultDetails.setFlag(true);

			}catch(Exception e){
				resultDetails.setFlag(false);
			}
			break;	

			case ENTERSTOREDEMAIL:
			try{
				value = tt.driver.utils.getValue(value);
				String UniqueEmail = tt.driver.hMap.get(value);
				webdriver.findElement(By.xpath(fieldText)).clear();
				webdriver.findElement(By.xpath(fieldText)).sendKeys(UniqueEmail);
				resultDetails.setFlag(true);
				}catch(Exception e){
					resultDetails.setFlag(false);
				}
			break;
			case ISNOTDISPLAYED:
		try {
			//WebElement isdis = webdriver.findElement(By.xpath(fieldText));
			
			if(!webdriver.findElement(By.xpath(fieldText)).isDisplayed())
			{System.out.println("Element is Visible");
}else{
System.out.println("Element is InVisible");
}
			resultDetails.setFlag(true);
		} catch (Exception ex) {
			resultDetails.setFlag(false);
			ex.printStackTrace();
			}
			break;

		case ISDISPLAYED:
		try {
			WebElement isdis = webdriver.findElement(By.xpath(fieldText));
			isdis.isDisplayed();
			resultDetails.setFlag(true);
		} catch (Exception ex) {
			resultDetails.setFlag(false);
			ex.printStackTrace();
			}
			break;
		
		case ISSELECTED:
		try {
			WebElement issel = webdriver.findElement(By.xpath(fieldText));
			issel.isSelected();
			resultDetails.setFlag(true);
		} catch (Exception ex) {
			resultDetails.setFlag(false);
			ex.printStackTrace();
		}
		break;

		case INPUTSUGGESTIONSELECTION:
		try{
			Actions actions=new Actions(webdriver);
			int convertValue = Integer.parseInt(tt.driver.utils.getValue(value));	
			for(int i=0; i<=convertValue; i++)
			{
				actions.sendKeys(Keys.DOWN).build().perform();
			}
			actions.sendKeys(Keys.ENTER).build().perform();
			resultDetails.setFlag(true);
			}
			catch(Exception e){
				resultDetails.setFlag(false);
				}
				break;   

				case VERIFYTEXT:
				try{
					WebElement element = webdriver.findElement(By.xpath(fieldText));
					String elementValue = element.getText();
					if(!elementValue.isEmpty())
						{
							resultDetails.setFlag(true); 
						}              
					}catch(Exception e){
						resultDetails.setFlag(false);
					}
					break;   
					
			case DRAGANDDROPFUNC:
				   try{
					String[] xpath=  fieldText.split(":", 2);				
				WebElement From=webdriver.findElement(By.xpath(xpath[0]));
				WebElement To=webdriver.findElement(By.xpath(xpath[1]));					
				Actions actions=new Actions(webdriver);							
				actions.dragAndDrop(From, To).build().perform();
				resultDetails.setFlag(true);
				   }
				   
				   catch(Exception e){
							resultDetails.setFlag(false);
						}
					break;
					
				case SWITCHTOFRAME:
				try{
					WebElement frameId = webdriver.findElement(By.xpath(fieldText));
					webdriver.switchTo().frame(frameId);
					resultDetails.setFlag(true);
					
				}catch(Exception e){
					 resultDetails.setFlag(false);
				}					
				  	break;
					
					case ALERTACCEPT:
				try{
					 Alert alert = webdriver.switchTo().alert();
					 Thread.sleep(1000);
					 alert.accept();
					resultDetails.setFlag(true);	
				}catch(Exception e){
					 resultDetails.setFlag(false);
				}					
				  	break;
					
					
				case ALERTDISMISS:
				try{
					 Alert alert = webdriver.switchTo().alert();
					 Thread.sleep(1000);
					 alert.dismiss();
					resultDetails.setFlag(true);	
				}catch(Exception e){
					 resultDetails.setFlag(false);
				}					
				  	break;
					
			case ALERTGETTEXT:
				try{
					 Alert alert = webdriver.switchTo().alert();
					 Thread.sleep(1000);
					 String alertValue=alert.getText();
					 if(!alertValue.isEmpty())
					 {
						resultDetails.setFlag(true); 
					 }	
				}catch(Exception e){
					 resultDetails.setFlag(false);
				}					
				  	break;
					
			case HOVERANDCLICK:
				
				try{

				WebElement element = webdriver.findElement(By.xpath(fieldText)); 
				Actions actions=new Actions(webdriver); 
				actions.moveToElement(element);
				actions.click().build().perform();
				resultDetails.setFlag(true);
				}

				catch(Exception e){
				resultDetails.setFlag(false);
				}
				break;

			case HOVER:
				try{

				WebElement element = webdriver.findElement(By.xpath(fieldText)); 
				Actions actions=new Actions(webdriver); 
				actions.moveToElement(element).build().perform();
				resultDetails.setFlag(true);
				}

				catch(Exception e){
				resultDetails.setFlag(false);
				}
				break;
				
			case COMPARETEXT:
				try{
								value = tt.driver.utils.getValue(value);
								String actualValue = webdriver.findElement(By.xpath(fieldText)).getText();
								if (value.equalsIgnoreCase(actualValue)|| value.contains(actualValue))
								{
								resultDetails.setFlag(true);
								}
				else
				{
					resultDetails.setFlag(false);             
				 }
				resultDetails.setFlag(true);
				}
				catch(Exception e){
				  resultDetails.setFlag(false);               
				}
				break;

			case CURRENTDATECHECK:
				try{
								String givedate = webdriver.findElement(By.xpath(fieldText)).getAttribute("value");
								SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
								Date date = new Date();
								
				if (givedate.equalsIgnoreCase(formatter.format(date)))
				{
				resultDetails.setFlag(true);      
				}

				else
				{
				resultDetails.setFlag(false);
				}
				resultDetails.setFlag(true);
				}catch(Exception e){
				resultDetails.setFlag(false);
				}
				break;
				
				case PAGEREFERSH:
				try{
				webdriver.navigate().to(webdriver.getCurrentUrl());
			    resultDetails.setFlag(true);      
				}catch(Exception e){
				resultDetails.setFlag(false);
				}
				break;
				
				case SPECIFICWAIT:
                   try{
                   WebDriverWait wait1 = new WebDriverWait(webdriver, 100);                                                               
					WebElement element = wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(fieldText)));
                   resultDetails.setFlag(true);
                   } catch (Exception ex) {
					resultDetails.setFlag(false);
					ex.printStackTrace();
					}
					break;


					case IMPWAIT:
						try{
						webdriver.manage().timeouts().implicitlyWait(100,TimeUnit.SECONDS);
                        webdriver.manage().timeouts().pageLoadTimeout(100,TimeUnit.SECONDS);
                                resultDetails.setFlag(true);
							} catch (Exception ex) {

                                  resultDetails.setFlag(false);

                                  ex.printStackTrace();

                           }

                          break;


		}
		} catch (IllegalArgumentException e) {
			resultDetails.setErrorMessage(" Unable to fine keyword "+action+"in AppTestType");
			System.out.println(e.getMessage());
		}catch(Exception e){
			resultDetails.setErrorMessage(" Exception in executing the action " + action);
			System.out.println(e.getMessage());
		}
		return resultDetails;
	}
	
	
	public static void FlightSelection(WebDriver webdriver, String fieldText, String value)
	{
		
		
		try
		{
		       wait = new WebDriverWait(webdriver, 60);
		
		String testdata[] = value.split("::");
		
		//Parameter 1

		String Carrier=testdata[0]; //Example: UA  LH
		
		//Parameter 2
		
		String AwardType=testdata[1];//Economysaver EconomyStandard		FirstSaver 		FirstStandard		BusinessStandard		BusinessSaver
		
		//Parameter 3
		
		int segmentcount=Integer.parseInt(testdata[2].split(":")[1]); // seg:0  seg:1 seg:2
		
		//Parameter 4
		
		String Market=testdata[3]; // domestic international
		
		//Parameter 5
		
		String TripType=testdata[4]; // OW RT MC
		
		webdriver.findElement(By.xpath("//label[contains(text(),'Detailed view')]")).click();
		
		boolean flightFound = false;
	
		for(int datechange=1;datechange<=10;datechange++)
		{
			
			try {
				Thread.sleep(6000);
			} catch (InterruptedException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			

			if(datechange>1)
			{
				webdriver.findElement(By.xpath("//a[contains(text(),'Edit search')]")).click();
				try {
					Thread.sleep(4000);
				Calendar calendar = Calendar.getInstance();

				Date startdate = new Date();

				SimpleDateFormat format = new SimpleDateFormat("MMM d,YYYY");
					
					
					format.format(startdate);
					
					
					int random = (int )(Math.random() * 200 + 10);
					System.out.println(random);
					random=random+60;
					calendar.add(Calendar.DAY_OF_MONTH, random);
					Date Startdate = calendar.getTime();		
					System.out.println("StartDate:"+format.format(Startdate));
					
					
					

					//Entering Today's Date
					webdriver.findElement(By.id("DepartDate")).clear();
					webdriver.findElement(By.id("DepartDate")).sendKeys(format.format(Startdate));

					
					
					
					/*webdriver.findElement(By.xpath("//*[@id='DepartDate']//following-sibling::button")).click();
					Thread.sleep(3000);
					webdriver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div[2]/table/tbody/tr[2]/td[5]/a")).click();*/

					if(TripType.contains("RT")||TripType.contains("MC")){
						
						calendar.add(Calendar.DAY_OF_MONTH, 3);
						Date returndate = calendar.getTime();
						System.out.println(format.format(returndate));

						//Entering Return Date +3 Days
						webdriver.findElement(By.id("ReturnDate")).clear();
						webdriver.findElement(By.id("ReturnDate")).sendKeys(format.format(returndate));


					}

					Thread.sleep(3000);
					webdriver.findElement(By.xpath("//*[@id='flightSearch']/div[2]/div[3]/button[2]")).click();
					wait.until(ExpectedConditions.presenceOfElementLocated(By
							.xpath("//*[@id='fl-results-loader-full']/div[1]")));
					wait.until(ExpectedConditions.elementToBeClickable(By
							.xpath("//*[@id='fl-search-header-wrap']/div[2]/a")));
					Thread.sleep(6000);



				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


			}

			if(segmentcount==0)
			{
				try{

					webdriver.findElement(By.id("filter_oneStop")).click();			

				}
				catch(Exception e)
				{

				}
				
				try
				{

					webdriver.findElement(By.id("filter_twoPlusStop")).click();
				}catch(Exception e)
				{

				}
			}
			else if(segmentcount==1)
			{
				try{

					try{
						webdriver.findElement(By.id("filter_nonStop")).click();
					}catch(Exception e)
					{

					}
					try{
						webdriver.findElement(By.id("filter_twoPlusStop")).click();
					}catch(Exception e)
					{

					}

				}
				catch(Exception e)
				{


				}
			}
			else if(segmentcount==2)
			{
				try{
					webdriver.findElement(By.id("filter_nonStop")).click();
								}
				catch(Exception e)
				{

				}
				try
				{
					webdriver.findElement(By.id("filter_oneStop")).click();	
				}catch(Exception e)
				{
				}
				}

			else
			{
				System.out.println("Please enter correct segement count");
			}


			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//********************** Sorting********************** 
			
			if(Market.equalsIgnoreCase("domestic"))
			{
			
				if(AwardType.equalsIgnoreCase("EconomySaver")||AwardType.equalsIgnoreCase("BusinessSaver")||AwardType.equalsIgnoreCase("EconomyStandard")||AwardType.equalsIgnoreCase("BusinessStandard"))
				{
					webdriver.findElement(By.id("column-MIN-ECONOMY-SURP-OR-DISP")).click();
					Thread.sleep(3000);
				}

				else if(AwardType.equalsIgnoreCase("FirstSaver"))
				{
					webdriver.findElement(By.id("column-BUSINESS-SURPLUS")).click();
					Thread.sleep(3000);
				}
				else if(AwardType.equalsIgnoreCase("FirstStandard"))
				{

					webdriver.findElement(By.id("column-BUSINESS-DISPLACEMENT")).click();
					Thread.sleep(3000);
				}
			}
			else if(Market.equalsIgnoreCase("international"))
			{
				if(AwardType.equalsIgnoreCase("EconomySaver")||AwardType.equalsIgnoreCase("EconomyStandard"))
				{
					webdriver.findElement(By.id("column-MIN-ECONOMY-SURP-OR-DISP")).click();
					Thread.sleep(3000);
				}
				
				else if(AwardType.equalsIgnoreCase("BusinessSaver"))
				{
					webdriver.findElement(By.id("column-BUSINESS-SURPLUS")).click();
					Thread.sleep(3000);
				}
				else if(AwardType.equalsIgnoreCase("BusinessStandard"))
				{

					webdriver.findElement(By.id("column-BUSINESS-DISPLACEMENT")).click();
					Thread.sleep(3000);
				}
				else if(AwardType.equalsIgnoreCase("FirstSaver"))
				{
					webdriver.findElement(By.id("column-FIRST-SURPLUS")).click();
					Thread.sleep(3000);
				}
				else if(AwardType.equalsIgnoreCase("FirstStandard"))
				{
					webdriver.findElement(By.id("column-FIRST-DISPLACEMENT")).click();
					Thread.sleep(3000);
				}
				else
				{
					System.out.println("Enter Proper Award Type");
				}
			}

			List<WebElement> rows = webdriver.findElements(By.xpath("//*[@id='fl-results']/div/ul/li"));
			int resultflightscount = rows.size();
			WebElement element;
			String flightFare="";
			int colNum = 0;
			String FlightCarrier="";
			int actualrow;
			for (actualrow = 1; actualrow <= resultflightscount; actualrow++) {
				
				
				
				
				//******************Flight Name************************
				if(segmentcount>0)
					try
				{
						FlightCarrier=webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[1]/ul/li[1]/div[2]")).getText();
				}catch(Exception e)
				{
					continue;
				}
				else
					try
				{
						FlightCarrier = webdriver.findElement(
								By.xpath("//*[@id='fl-results']/div/ul/li[" + actualrow	+ "]/div[1]/ul/li/div[1]")).getText();
				}catch(Exception e)
				{
					continue;
				}


				if(FlightCarrier.toUpperCase().contains(Carrier.toUpperCase()))
				{			

					try
					{

						//*******************Flight Selecting*************************

						if(Market.equalsIgnoreCase("domestic"))
						{
							if(AwardType.equalsIgnoreCase("EconomySaver")||AwardType.equalsIgnoreCase("BusinessSaver"))
							{
								
								
								
								
								
								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(0);

								flightFare = element.getText(); //Select fare for Economy (lowest) Saver Award 12.5k miles +US $5.60 Select

								if(flightFare.contains("Not available"))
									continue;
								if(!flightFare.contains("Saver Award"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}
							}

							else if(AwardType.equalsIgnoreCase("EconomyStandard")||AwardType.equalsIgnoreCase("BusinessStandard"))
							{
							
								
								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(0);

								flightFare = element.getText();//Select fare for Economy (lowest) Saver Award 12.5k miles +US $5.60 Select
								if(flightFare.contains("Not available"))
									continue;
								if(flightFare.contains("Saver Award")){
									continue;
								}
								else
									JClick(webdriver,element); //element.click();
								flightFound=true;
								break;
									

							}
							else if(AwardType.equalsIgnoreCase("FirstSaver"))
							{
								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(1);

								flightFare = element.getText();

								if(flightFare.contains("Not available"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}

							}



							else if(AwardType.equalsIgnoreCase("FirstStandard"))
							{
								

								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(2);

								flightFare = element.getText();

								if(flightFare.contains("Not available"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}

							}	

							else
							{
								System.out.println("Plese select the correct Award Type");
							}

						}



						else if(Market.equalsIgnoreCase("international"))
						{
							if(AwardType.equalsIgnoreCase("EconomySaver"))
							{
								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(0);

								flightFare = element.getText();
								if(flightFare.contains("Not available"))
									continue;
								if(!flightFare.contains("Saver Award"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}
							}

							else if(AwardType.equalsIgnoreCase("EconomyStandard"))
							{

								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(0);

								flightFare = element.getText();
								if(flightFare.contains("Not available"))
									continue;
								if(flightFare.contains("Saver Award")){
									continue;
								}
								else
									
									JClick(webdriver,element); //element.click();
								flightFound=true;
								break;

							}

							else if(AwardType.equalsIgnoreCase("BusinessSaver"))
							{
								
								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(1);

								flightFare = element.getText();
								if(flightFare.contains("Not available"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}

							}
							else if(AwardType.equalsIgnoreCase("BusinessStandard"))
							{
								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(2);

								flightFare = element.getText();
								if(flightFare.contains("Not available"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}

							}



							else if(AwardType.equalsIgnoreCase("FirstSaver"))
							{
								
								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(3);

								flightFare = element.getText();
								if(flightFare.contains("Not available"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}

							}
							


							else if(AwardType.equalsIgnoreCase("FirstStandard"))
							{

								WebElement formElement = webdriver.findElement(By.xpath("//ul[@class='flight-result-list']/li["+actualrow+"]/div[2]"));
								List<WebElement> allFormChildElements = formElement.findElements(By.xpath("*"));
								element=allFormChildElements.get(4);

								flightFare = element.getText();
								if(flightFare.contains("Not available"))
									continue;
								else{
									JClick(webdriver,element); //element.click();
									flightFound=true;
									break;
								}

							}	

							else
							{
								System.out.println("Plese select the correct Award Type");
							}

						}

					
					}catch(Exception e)
					{
						System.out.println("Price for the Flight is Not Available");
						e.printStackTrace();
					}



				}



			}

			// *****************NOT FOUND NOTIFICATION**************
			if(flightFound)
				break;
			if(datechange==10)
			{
				if(actualrow==resultflightscount+1)
				if(!flightFound)
				{
					
					System.out.println("Unable to find Flights as per your inputs!!!");
					resultDetails.setErrorMessage("No flights are available for your selection");
					resultDetails.setFlag(false);
				}
			}

		}
		
		resultDetails.setFlag(true);

	} catch (Exception ex) {
		resultDetails.setFlag(false);
		ex.printStackTrace();
	}




	}






	public static void JClick(WebDriver webdriver, WebElement element)
	{
		try{
			JavascriptExecutor executor = (JavascriptExecutor)webdriver;
			executor.executeScript("arguments[0].click();", element);
		}
		catch(Exception e)
		{

		}
	}
	
	


}

	

	

